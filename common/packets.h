#ifndef packets_H
#define packets_H

typedef struct {
	uint8_t status;
	uint8_t key;
	uint8_t velocity;
} Note_packet;

#endif

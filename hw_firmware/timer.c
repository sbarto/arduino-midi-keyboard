#include <avr/interrupt.h>

#include "timer.h"

#define TIMER_PERIOD_MS (20) //timer period for debouncing

volatile uint8_t timer_trigger = FALSE;

ISR(TIMER5_COMPA_vect){
  timer_trigger = TRUE;
}

void timer_init(void){ //for debouncing
  // configure timer
  // set the prescaler to 1024
  TCCR5A = 0;
  TCCR5B = (1 << WGM52) | (1 << CS50) | (1 << CS52); 

  // at this count rate
  // 1 ms will correspond do 15.62 counts
  uint16_t ocrval=(uint16_t)(15.62*TIMER_PERIOD_MS);

  OCR5A = ocrval;

  // clear int
  cli();
  TIMSK5 |= (1 << OCIE5A);  // enable the timer interrupt
  // enable int
  sei();
}

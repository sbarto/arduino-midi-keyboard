#include "UART.h"
#include "timer.h"
#include "midi_keyboard.h"

int main(void){
	struct UART* uart = UART_init();
	timer_init();
	keyboard_init();
	key_event events[MAX_EVENTS];
	while(1){
		if (timer_trigger){
			timer_trigger = FALSE;
			uint8_t num_events = key_scan(events);
			for (uint8_t k=0; k<num_events; ++k){
				key_event e = events[k];
				send_midi_message(e.status, e.key, MAX_VELOCITY);
			}
			flush_note_buffer(uart);
		}
	}	
}


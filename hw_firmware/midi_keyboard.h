#ifndef MIDI_KEYBOARD_H
#define MIDI_KEYBOARD_H

#include "UART.h"

#define NOTE_ON 144  //10010000 in binary, note on command on channel 1
#define NOTE_OFF 128  //10000000 in binary, note off command on channel 1
#define MAX_VELOCITY 127
#define MAX_EVENTS 16

typedef struct {
  uint8_t status: 1; //  first bit: 1=pressed, 0=released
  uint8_t key:   7; //  key number (between 0 and 16)
} key_event;

void keyboard_init(void);
uint8_t key_scan(key_event* events);
void send_midi_message(uint8_t status, uint8_t key, uint8_t velocity);
void flush_note_buffer(struct UART* uart);

#endif


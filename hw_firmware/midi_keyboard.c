#include <util/delay.h>
#include <string.h>

#include "midi_keyboard.h"
#include "../common/packets.h"

#define NOTE_BUFFER_SIZE 128

Note_packet note_buffer[NOTE_BUFFER_SIZE];
uint8_t note_buffer_end;

uint16_t key_status; // this is the full status of our keyboard, 1 bit per key

uint8_t key_num_to_note[15] = {
  60, //do
  62, //re
  64, //mi
  65, //fa
  67, //sol
  69, //la
  71, //si
  72, //DO
  61, //do#
  63, //re#
  0, //null
  0, //null
  66, //fa#
  68, //sol#
  70, //la#
};

void keyboard_init(void){
  DDRA=0xF0;  // 4 most significant bits as output, rest as input;
  PORTA=0x0F; // pull up on input bits
  
  note_buffer_end=0;
}

uint8_t key_scan(key_event* events){
  uint16_t new_status=0; // new key status
  int num_events=0;      // number of events
  uint8_t key_num=0;         // key number (0..16)
  for (int r=0; r<4; ++r){
    // this is the mask for the output
    // all 1 and a 0 in position r+4
    uint8_t row_mask= ~(1<<(r+4));
    
    // we zero the row r
    PORTA =row_mask;

    // wait 100 us to stabilize signal
    _delay_us(100);

    //read first 4  bit (and negate them)
    // we will have a 1 in correpondence
    // of the key pressed on that row
    uint8_t row= ~(0x0F & PINA);
    
    for (int c=0;c<4;++c){

      uint16_t key_mask=1<<key_num;
      uint16_t col_mask=1<<c;
      
      // 1 if key pressed
      uint8_t cs=(row & col_mask)!=0; 
      
      // if key pressed toggle to 1
      // the corresoinding bit
      // of the new keymap
      if (cs){
	new_status |= key_mask;
      }
      
      //fetch the previous status of that key
      uint8_t ps=(key_mask&key_status)!=0;

      // if different from before, shoot an event;
      if (cs!=ps){
	key_event e;
	e.key=key_num;
	e.status = cs;
	events[num_events]=e;
	++num_events;
      }
      ++key_num;
    }
    row_mask=row_mask<<1;
  }
  key_status=new_status;
  return num_events;
}

void put_note(Note_packet* note){
  memcpy((void*)(note_buffer+note_buffer_end), (void*)note, sizeof(Note_packet));
  note_buffer_end++;
}

void send_midi_message(uint8_t status, uint8_t key, uint8_t velocity){
  Note_packet packet;
  
  if(status) packet.status = NOTE_ON;
  else packet.status = NOTE_OFF;
  packet.key = key_num_to_note[key];
  packet.velocity = velocity;
  
  put_note(&packet);
}

void flush_note_buffer(struct UART* uart){
  for(int i = 0; i<note_buffer_end; i++){
    UART_putChar(uart, note_buffer[i].status);
    UART_putChar(uart, note_buffer[i].key);
    UART_putChar(uart, note_buffer[i].velocity);
  }
  note_buffer_end=0;
}


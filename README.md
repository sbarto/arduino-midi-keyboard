# Arduino MIDI keyboard

Il programma è composto da due parti: 

 - una che gira sull'arduino e che invia messaggi MIDI al computer relativi ai tasti premuti su una tastiera musicale di una ottava realizzata attraverso una tastiera a matrice 4x4

 - l'altra che gira sul computer e che riproduce le note corrispondenti ai messaggi MIDI ricevuti.

# Funzionamento hw_firmware

Il funzionamento del programma sull'arduino è scandito da un **timer** che ogni 20 millisecondi invia una **interruzione**, riattivando una funzione che analizza lo stato della tastiera per verificare se è cambiato. Per ogni tasto il cui stato è cambiato l'arduino invia il messaggio MIDI corrispodente sulla **seriale**.
Le righe e le colonne della tastiera sono connesse all'arduino come mostrato in fiugra

![Circuito](circuit.png)

# Funzionamento pc_client

Il client sul computer è composto da tre thread:

 - **UI** gestisce l'interfaccia sul terminale
 - **MIDI_INPUT** legge i messaggi MIDI sulla seriale
 - **SOUND_OUTPUT** riproduce le note
 
# Compilazione ed esecuzione

Nella cartella hw_firmware è presente il codice per l'arduino.
 - Comando per compilare:
 > make

 - Comando per flashare:
 > make main.hex

Nella cartella pc_client è presente il codice per il pc.
 - Comando per compilare:
 > make

 - Comando per eseguire:
 > ./pc_client
	

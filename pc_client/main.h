#ifndef MAIN_H
#define MAIN_H

#include <semaphore.h>
sem_t note_sem;
sem_t exit_sem;

char exit_program;

int sound_output();
int midi_input();
int ui();

#endif


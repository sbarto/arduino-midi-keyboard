#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "main.h"

char exit_program = 0;

int main(){
	pthread_t sound_thread;
	pthread_t midi_thread;
	pthread_t ui_thread;
	
	if(sem_init(&note_sem, 0, 1)!=0){
		perror("error initializing semaphore note_sem");
		return EXIT_FAILURE;
	}
	
	if(sem_init(&exit_sem, 0, 1)!=0){
		perror("error initializing semaphore exit_sem");
		return EXIT_FAILURE;
	}

	if(pthread_create(&sound_thread, NULL, (void *) sound_output, NULL)!=0){
		perror("error creating sound_thread");
		return EXIT_FAILURE;
	}
	if(pthread_create(&midi_thread, NULL, (void *) midi_input, NULL)!=0){
		perror("error creating midi_thread");
		return EXIT_FAILURE;
	}
	if(pthread_create(&ui_thread, NULL, (void *) ui, NULL)!=0){
		perror("errorcreating ui_thread");
		return EXIT_FAILURE;
	}
	
	int ret_val[3];

	if(pthread_join(sound_thread, (void*) &ret_val[0])!=0){
		perror("error joining sound_thread");
		return EXIT_FAILURE;
	}
	if(pthread_join(midi_thread, (void*) &ret_val[1])!=0){
		perror("error joining midi_thread");
		return EXIT_FAILURE;
	}
	if(pthread_join(ui_thread, (void*) &ret_val[2])!=0){
		perror("error joining ui_thread");
		return EXIT_FAILURE;
	}

	for(int i=0; i<3; i++){
		if(ret_val[i] == EXIT_FAILURE) return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


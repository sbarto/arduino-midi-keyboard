#include <alsa/asoundlib.h>
#include <math.h>
#include <stdlib.h>

#include "main.h"
#include "sound_output.h"

#define SAMPLE_RATE 44100

snd_pcm_t* handle = NULL;

note_type* note_list[NOTES] = {NULL};

const float step = 1/(float)SAMPLE_RATE;

float output_buffer[BUFFER_SIZE];

static int error_handler(){
	//we close alsa if open
	if(handle!=NULL){
    int err = snd_pcm_drain(handle);
    if (err < 0) printf("error during snd_pcm_drain: %s\n", snd_strerror(err));

    err = snd_pcm_close(handle);
    if (err < 0) printf("error during snd_pcm_close: %s\n", snd_strerror(err));
      
    err = snd_config_update_free_global();
    if (err < 0) printf("error during snd_config_update_free_global: %s\n", snd_strerror(err));
  }
	
	//we tell other threads to stop
	if (sem_wait(&exit_sem) != 0) perror("error during sem_wait");
	exit_program=1;
	if (sem_post(&exit_sem) != 0) perror("error during sem_post");
	
	return EXIT_FAILURE;
}

float id_to_freq(int id) {
  return pow(2, (id - 69)/12.0) * 440.0;
}

//wave generator
float phase_to_square(float p){
  return p < .5 ? -1 : 1;
}

void new_note(int id){
  note_type* n = malloc(sizeof(note_type));
  n->freq = id_to_freq(id);
  n->hold = 1;
  n->phase = 0.0;
  n->velocity = 0.0;
  note_list[id] = n;
}

void hold_note(int id){
  assert(note_list[id] != NULL);
  note_list[id]->hold = 1;
}

void set_velocity(int id, char velocity){
  assert(note_list[id] != NULL);
  note_list[id]->velocity = velocity;
}

void drop_note(int id){
  assert(note_list[id] != NULL);
  note_list[id]->hold = 0;
}

void free_note(int id){
  assert(note_list[id] != NULL);
  free(note_list[id]);
  note_list[id] = NULL;
}

int sound_output(){
  //alsa init
  int err = snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
  if (err < 0){
    printf("error during snd_pcm_open: %s\n", snd_strerror(err));
    return error_handler();
  }
  err = snd_pcm_set_params(handle, SND_PCM_FORMAT_FLOAT,
  SND_PCM_ACCESS_RW_INTERLEAVED, 1, SAMPLE_RATE, 0, 1 << 16);
  if (err < 0){
    printf("error during snd_pcm_set_params: %s\n", snd_strerror(err));
    return error_handler();
  }

  
	while (1){
		int stop=0;
		
		if (sem_wait(&exit_sem) != 0){
			perror("error during sem_wait");
			return error_handler();
		}
		
		if(exit_program==1) stop=1;
		
		if (sem_post(&exit_sem) != 0){
			perror("error during sem_post");
			return error_handler();
		}
		
		if(stop) break;
    
    
    //clean buffer
    for (int i = 0; i<BUFFER_SIZE; i++) output_buffer[i] = 0.0;

    if (sem_wait(&note_sem) != 0){
			perror("error during sem_wait");
			return error_handler();
		}

    for (int id = 0; id<NOTES; id++){
      if (note_list[id] != NULL){
        note_type* n = note_list[id];
        for (int i = 0; i<BUFFER_SIZE; i++){
          //mute dropped notes
          if (!n->hold) n->velocity = 0.0;

          //update phase
          n->phase += n->freq * step;
          while (n->phase >= 1) n->phase--;

          //generate wave
          n->sample[i] = n->velocity * phase_to_square(n->phase);

          output_buffer[i] += n->sample[i]/1000;
        }
      }
    }

    if (sem_post(&note_sem) != 0){
			perror("error during sem_post");
			return error_handler();
		}

    //write buffer
    err = snd_pcm_writei(handle, output_buffer, BUFFER_SIZE);
    if (err < 0){
      printf("error during snd_pcm_writei: %s\n", snd_strerror(err));
      return error_handler();
    }
  }
  
  //free notes
  for (int id = 0; id<NOTES; id++){
    if (note_list[id] != NULL) free_note(id);
  }

  int exit_status = EXIT_SUCCESS;

  //exit alsa
  err = snd_pcm_drain(handle);
  if (err < 0){
    printf("error during snd_pcm_drain: %s\n", snd_strerror(err));
    exit_status = EXIT_FAILURE; //we are already exiting, no need to tell the other threads to stop
  }
  
  err = snd_pcm_close(handle);
  if (err < 0){
    printf("error during snd_pcm_close: %s\n", snd_strerror(err));
    exit_status = EXIT_FAILURE;
  }
  
  err = snd_config_update_free_global();
  if (err < 0){
    printf("error during snd_config_update_free_global: %s\n", snd_strerror(err));
    exit_status = EXIT_FAILURE;
  }
  
  return exit_status;
}

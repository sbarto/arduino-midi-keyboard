#include <stdint.h>
#include <stdio.h>
#include <termios.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/select.h>

#include "main.h"
#include "midi_input.h"
#include "sound_output.h"
#include "../common/packets.h"

#define NOTE_ON 144		//10010000 in binary, note on command on channel 1
#define NOTE_OFF 128	//10000000 in binary, note off command on channel 1

int serial_set_interface_attribs(int fd, int speed, int parity){
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0){
		perror("error from tcgetattr");
		return 0;
	}

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);
	cfmakeraw(&tty);

	// enable reading
	tty.c_cflag &= ~(PARENB | PARODD);					// shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;		// 8-bit chars

	if (tcsetattr (fd, TCSANOW, &tty) != 0){
		perror("error from tcsetattr");
		return 0;
	}
	
	//clean buffer
	usleep(10000);
	tcflush(fd,TCIOFLUSH);
	
	return 1;
}

int midi_open(int* fd){
	*fd = open("/dev/ttyACM0", O_RDONLY | O_NOCTTY | O_SYNC );
	if (*fd<0){
		perror("error opening serial");
		fd=0;
		return 0;
	}
	return serial_set_interface_attribs(*fd, B19200, 0);
}

int midi_close(int* fd){
	if (close(*fd)){
		perror("error closing serial");
		return 0;
	}
	return 1;
}

static int error_handler(int* fd){
	//if open we close the serial
	if(fcntl(*fd, F_GETFL) != -1 || errno != EBADF) midi_close(fd);
	
	//we tell other threads to stop
	if (sem_wait(&exit_sem) != 0) perror("error during sem_wait");
	exit_program=1;
	if (sem_post(&exit_sem) != 0) perror("error during sem_post");
	
	return EXIT_FAILURE;
}

int read_note(int fd, Note_packet* note){
	
	fd_set set;
	struct timeval timeout;

	timeout.tv_sec = 0;
	timeout.tv_usec = 1000;
	FD_ZERO(&set);
	FD_SET(fd, &set);
	int res = select(FD_SETSIZE, &set, NULL, NULL, &timeout);
  if(res==-1){
		perror("error during select");
		return -1;
	}
	if(res==0) return 0;
	
	unsigned char status;
	if(read(fd, &status, sizeof(char))<0){
		perror("error reading message status");
		return -1;
	}

	if(status==NOTE_ON || status==NOTE_OFF){
		
		unsigned char key, velocity;
		if(read(fd, &key, sizeof(char))<0){
			perror("error reading message key");
			return -1;
		}
		if(read(fd, &velocity, sizeof(char))<0){
			perror("error reading message velocity");
			return -1;
		}

		note->status = status;
		note->key = key;
		note->velocity = velocity;
		
		return 1;
	} 
	return 0;
}

int midi_input(){
	
	//serial file descriptor
	int fd = 0;
	
	if(!midi_open(&fd)) return error_handler(&fd);

	while (1){
		int stop=0;
		
		//exit check
		if (sem_wait(&exit_sem) != 0){
			perror("error during sem_wait");
			return error_handler(&fd);
		}
		
		if(exit_program==1) stop=1;
		
		if (sem_post(&exit_sem) != 0){
			perror("error during sem_post");
			return error_handler(&fd);
		}
		
		if(stop) break;
		
		Note_packet note;
		int res = read_note(fd, &note);
		if(res==0) continue;
		
		if(res==-1) return error_handler(&fd);
		
		if (sem_wait(&note_sem) != 0){
			perror("error during sem_wait");
			return error_handler(&fd);
		}
		
		if (note.status == NOTE_ON){
			if(note_list[note.key]==NULL){
				new_note(note.key);
			} else {
				hold_note(note.key);
			}
			set_velocity(note.key, note.velocity);
		} else {
			drop_note(note.key);
		}

		if (sem_post(&note_sem) != 0){
			perror("error during sem_post");
			return error_handler(&fd);
		}
	}
	
	if(!midi_close(&fd)) return EXIT_FAILURE; //we are already exiting, no need to tell the other threads to stop
	
	return EXIT_SUCCESS;
}



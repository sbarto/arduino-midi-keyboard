#include "main.h"
#include "ui.h"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>

static int error_handler(){
	//we tell other threads to stop
	if (sem_wait(&exit_sem) != 0) perror("error during sem_wait");
	exit_program=1;
	if (sem_post(&exit_sem) != 0) perror("error during sem_post");
	
	return EXIT_FAILURE;
}

char getch() {
	char buf = 0;
	struct termios old = { 0 };
	fflush(stdout);
	if (tcgetattr(0, &old) < 0) perror("tcsetattr()");
	old.c_lflag    &= ~ICANON;   // local modes = Non Canonical mode
	old.c_lflag    &= ~ECHO;     // local modes = Disable echo. 
	old.c_cc[VMIN]  = 0;
	old.c_cc[VTIME] = 1;         // control chars (TIME value) = 1 (10 ms timeout)
	if (tcsetattr(0, TCSANOW, &old) < 0) perror("tcsetattr ICANON");
	if (read(0, &buf, 1) < 0) perror("read()");
	old.c_lflag    |= ICANON;    // local modes = Canonical mode
	old.c_lflag    |= ECHO;      // local modes = Enable echo. 
	if (tcsetattr(0, TCSADRAIN, &old) < 0) perror ("tcsetattr ~ICANON");
	return buf;
 }


int ui(){
	printf("PRESS ANY KEY ON THE MIDI KEYBOARD TO PLAY A NOTE\n");
	printf("PRESS 'Q' ON YOUR PC KEYBOARD TO QUIT THE PROGRAM\n"); 

 	while(1) {
		int stop=0;
		
		if (sem_wait(&exit_sem) != 0){
			perror("error during sem_wait");
			return error_handler();
		}
		
		if(exit_program==1) stop=1;
		
		if (sem_post(&exit_sem) != 0){
			perror("error during sem_post");
			return error_handler();
		}
		
		if(stop) break;
		
		char key = getch();
		
		if (key =='q'){
			if (sem_wait(&exit_sem) != 0){
				perror("error during sem_wait");
				return error_handler();
			}
			
			exit_program = 1;
			
			if (sem_post(&exit_sem) != 0){
				perror("error during sem_post");
				return error_handler();
			}
			break;
		}
	}
	
	return EXIT_SUCCESS;
}


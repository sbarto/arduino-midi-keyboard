#ifndef SOUND_OUTPUT_H
#define SOUND_OUTPUT_H

#define NOTES 128
#define BUFFER_SIZE (1 << 10)

typedef struct {
  float phase;
  float sample[BUFFER_SIZE];
  float freq;
  float velocity;
  char hold;
} note_type;

extern note_type* note_list[NOTES];

void new_note(int id);
void drop_note(int id);
void hold_note(int id);
void set_velocity(int id, char energy);

#endif

